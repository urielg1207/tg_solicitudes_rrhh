import 'package:flutter/material.dart';

import 'package:tg_solicitudes_rrhh/bloc/provider.dart';
import 'package:tg_solicitudes_rrhh/pages/datos_laborales_page.dart';
import 'package:tg_solicitudes_rrhh/pages/datos_personales_page.dart';

import 'package:tg_solicitudes_rrhh/pages/home_page.dart';
import 'package:tg_solicitudes_rrhh/pages/login_page.dart';
import 'package:tg_solicitudes_rrhh/pages/opciones_page.dart';
import 'package:tg_solicitudes_rrhh/pages/permisos_page.dart';
import 'package:tg_solicitudes_rrhh/pages/solicitudes_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        initialRoute: 'login',
        routes: {
          'login': (BuildContext context) => LoginPage(),
          'home': (BuildContext context) => HomePage(),
          'opciones': (BuildContext context) => OpcionesPage(),
          'datospersonales': (BuildContext context) => DatosPersonalesPage(),
          'datoslaborales': (BuildContext context) => DatosLaboralesPage(),
          'solicitudes': (BuildContext context) => SolicitudesPage(),
          'permisos': (BuildContext context) => PermisosPage(),
        },
        theme: ThemeData(
          primaryColor: Colors.deepPurple,
        ),
      ),
    );
  }
}
