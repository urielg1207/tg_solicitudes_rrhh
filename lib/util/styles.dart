import 'package:flutter/material.dart';

class StylesTG {
  static const primaryTextStyle = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.bold,
    color: Colors.black12,
  );

  static const secondaryTextStyle =
      TextStyle(fontSize: 28, fontWeight: FontWeight.bold, color: Colors.grey);
}
