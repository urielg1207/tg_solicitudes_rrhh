import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

// Colores institucionales
Color colorAzulOscuro = HexColor("#18527F");
Color colorAzulClaro = HexColor("#1A81B1");
Color colorAzulBotones = HexColor("#3BA8DE");
Color colorVerdeBotones = HexColor("#8DC63F");
Color colorGris = HexColor("#808080");

// Colores primarios básicos
Color colorBlanco = HexColor("#FFFFFF");
Color colorNegro = HexColor("#000000");
