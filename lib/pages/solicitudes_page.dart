import 'package:flutter/material.dart';
import 'package:tg_solicitudes_rrhh/bloc/provider.dart';
import 'package:tg_solicitudes_rrhh/util/colors.dart';

class SolicitudesPage extends StatelessWidget {
  int _selectedIndex = 2;

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of(context);

    return Scaffold(
        appBar: AppBar(
          title: Text('Solicitudes'),
          backgroundColor: colorAzulOscuro,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Email: ${bloc.email}'),
            Divider(),
            Text('Password: ${bloc.password}')
          ],
        ),
        bottomNavigationBar: _bottomNavigationBar(context));
  }

  Widget _bottomNavigationBar(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
          canvasColor: colorAzulOscuro,
          primaryColor: colorBlanco,
          textTheme: Theme.of(context).textTheme.copyWith(
              caption: TextStyle(color: Color.fromRGBO(116, 117, 152, 1.0)))),
      child: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
              icon: Icon(
                Icons.person_pin_rounded,
                size: 30.0,
                color: colorGris,
              ),
              title: Container()),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.supervised_user_circle,
                size: 30.0,
                color: colorGris,
              ),
              title: Container()),
          BottomNavigationBarItem(
              icon: Icon(Icons.assignment_rounded,
                  size: 30.0, color: colorBlanco),
              title: Container()),
          BottomNavigationBarItem(
              icon: Icon(Icons.watch_later_sharp, size: 30.0, color: colorGris),
              title: Container()),
        ],
        onTap: (int index) {
          // Datos personales
          if (index == 0) {
            Navigator.pushNamed(context, 'datospersonales');
          }

          // Datos laborales
          if (index == 1) {
            Navigator.pushNamed(context, 'datoslaborales');
          }

          // Solicitudes
          if (index == 2) {
            Navigator.pushNamed(context, 'solicitudes');
          }

          // Permisos
          if (index == 3) {
            Navigator.pushNamed(context, 'permisos');
          }
        },
        currentIndex: _selectedIndex,
        selectedItemColor: colorBlanco,
      ),
    );
  }
}
