import 'package:flutter/material.dart';

import 'dart:math';
import 'dart:ui';
import 'package:tg_solicitudes_rrhh/util/colors.dart';

class OpcionesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _fondoApp(),
          SingleChildScrollView(
            child: Column(
              children: <Widget>[_titulos(), _botonesRedondeados(context)],
            ),
          )
        ],
      ),
      //bottomNavigationBar: _bottomNavigationBar(context)
    );
  }

  Widget _fondoApp() {
    final gradiente = Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: FractionalOffset(0.0, 0.6),
              end: FractionalOffset(0.0, 1.0),
              colors: [colorBlanco, colorBlanco])),
    );

    final cajaInstitucional = Transform.rotate(
        angle: -pi / 5.0,
        child: Container(
          height: 360.0,
          width: 360.0,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(80.0),
              gradient:
                  LinearGradient(colors: [colorAzulOscuro, colorAzulOscuro])),
        ));

    return Stack(
      children: <Widget>[
        gradiente,
        Positioned(top: -100.0, child: cajaInstitucional)
      ],
    );
  }

  Widget _titulos() {
    return SafeArea(
      child: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text('TopGroup',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold)),
            SizedBox(height: 10.0),
            Text(
                'Soluciones de software innovadoras con fuerte especialización en Retail',
                style: TextStyle(color: Colors.white, fontSize: 18.0)),
          ],
        ),
      ),
    );
  }

  Widget _bottomNavigationBar(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
          canvasColor: colorAzulClaro,
          primaryColor: colorBlanco,
          textTheme: Theme.of(context).textTheme.copyWith(
              caption: TextStyle(color: Color.fromRGBO(116, 117, 152, 1.0)))),
      child: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.person_pin_rounded, size: 30.0),
              title: Container()),
          BottomNavigationBarItem(
              icon: Icon(Icons.supervised_user_circle, size: 30.0),
              title: Container()),
          BottomNavigationBarItem(
              icon: Icon(Icons.assignment_rounded, size: 30.0),
              title: Container()),
          BottomNavigationBarItem(
              icon: Icon(Icons.watch_later_sharp, size: 30.0),
              title: Container()),
        ],
      ),
    );
  }

  Widget _botonesRedondeados(BuildContext context) {
    return Table(
      children: [
        TableRow(children: [
          _crearBotonRedondeado(colorAzulBotones, Icons.person_pin_rounded,
              'Datos Personales', context, 'datospersonales'),
          _crearBotonRedondeado(colorAzulBotones, Icons.supervised_user_circle,
              'Datos Laborales', context, 'datoslaborales'),
        ]),
        TableRow(children: [
          _crearBotonRedondeado(colorAzulBotones, Icons.assignment_rounded,
              'Solicitudes', context, 'solicitudes'),
          _crearBotonRedondeado(colorAzulBotones, Icons.watch_later_sharp,
              'Permisos', context, 'permisos'),
        ])
      ],
    );
  }

  Widget _crearBotonRedondeado(Color color, IconData icono, String texto,
      BuildContext context, String opcion) {
    return ClipRect(
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
        child: Container(
            height: 180.0,
            margin: EdgeInsets.all(15.0),
            decoration: BoxDecoration(
                color: Color.fromRGBO(62, 66, 107, 0.7),
                borderRadius: BorderRadius.circular(20.0)),
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  SizedBox(height: 5.0),
                  CircleAvatar(
                    backgroundColor: color,
                    radius: 35.0,
                    child: Icon(icono, color: Colors.white, size: 40.0),
                  ),
                  Text(texto, style: TextStyle(color: colorBlanco)),
                  SizedBox(height: 5.0)
                ],
              ),
              color: Color.fromRGBO(62, 66, 107, 0.7),
              onPressed: () => Navigator.pushNamed(context, opcion),
            )),
      ),
    );
  }
}
