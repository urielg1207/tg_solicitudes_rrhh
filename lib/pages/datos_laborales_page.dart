import 'package:flutter/material.dart';
import 'package:tg_solicitudes_rrhh/bloc/provider.dart';
import 'package:tg_solicitudes_rrhh/util/colors.dart';

class DatosLaboralesPage extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<DatosLaboralesPage> {
  int _selectedIndex = 1;

  String _nombre = "";
  String _email = "";
  String _fecha = "";
  List<String> _cargos = ['Analista', 'Desarrollador', 'Líder', 'Técnico'];

  String _opcionSeleccionada = "Analista";

  TextEditingController _inputFieldDateController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of(context);

    return Scaffold(
        appBar: AppBar(
          title: Text('Datos Laborales'),
          backgroundColor: colorAzulOscuro,
        ),
        body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
          children: [
            _crearDropdownCargos(),
            Divider(),
            _crearFechaIngreso(context),
            Divider(),
            _crearInputvacaciones()
          ],
        ),
        bottomNavigationBar: _bottomNavigationBar(context));
  }

  Widget _crearInputvacaciones() {
    return TextField(
      //autofocus: true,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
          hintText: "Vacaciones Disponibles",
          labelText: "Vacaciones",
          suffixIcon: Icon(Icons.accessibility),
          icon: Icon(Icons.account_circle),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))),
      onChanged: (valor) {
        setState(() {
          _nombre = valor;
        });
      },
    );
  }

  Widget _crearEmail() {
    return TextField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          hintText: "Email",
          labelText: "Email",
          suffixIcon: Icon(Icons.alternate_email),
          icon: Icon(Icons.email),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))),
      onChanged: (valor) {
        setState(() {
          _email = valor;
        });
      },
    );
  }

  Widget _crearPassword() {
    return TextField(
      obscureText: true,
      decoration: InputDecoration(
          hintText: "Password",
          labelText: "password",
          suffixIcon: Icon(Icons.lock_open),
          icon: Icon(Icons.lock),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))),
      onChanged: (valor) {
        setState(() {
          _email = valor;
        });
      },
    );
  }

  Widget _crearFechaIngreso(BuildContext buildContext) {
    return TextField(
      controller: _inputFieldDateController,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
          hintText: "Fecha Ingreso",
          labelText: "fecha Ingreso",
          suffixIcon: Icon(Icons.perm_contact_calendar),
          icon: Icon(Icons.calendar_today),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(20.0))),
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate(buildContext);
      },
    );
  }

  void _selectDate(BuildContext buildContext) async {
    DateTime picked = await showDatePicker(
        context: buildContext,
        initialDate: DateTime.now(),
        firstDate: DateTime(2018),
        lastDate: DateTime(2025),
        locale: Locale('es', 'ES'));

    if (picked != null) {
      setState(() {
        _fecha = picked.toString();
        _inputFieldDateController.text = _fecha;
      });
    }
  }

  Widget _crearDropdownCargos() {
    return Row(
      children: [
        SizedBox(
          width: 30.0,
        ),
        Expanded(
          child: DropdownButton(
              value: _opcionSeleccionada,
              items: _getOpcionesDropdown(),
              onChanged: (opcion) {
                setState(() {
                  _opcionSeleccionada = opcion;
                });
              }),
        )
      ],
    );
  }

  List<DropdownMenuItem<String>> _getOpcionesDropdown() {
    List<DropdownMenuItem<String>> lista = new List();

    _cargos.forEach((cargo) {
      lista.add(DropdownMenuItem(
        child: Text(cargo),
        value: cargo,
      ));
    });

    return lista;
  }

  Widget _bottomNavigationBar(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
          canvasColor: colorAzulOscuro,
          primaryColor: colorBlanco,
          textTheme: Theme.of(context).textTheme.copyWith(
              caption: TextStyle(color: Color.fromRGBO(116, 117, 152, 1.0)))),
      child: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
              icon: Icon(
                Icons.person_pin_rounded,
                size: 30.0,
                color: colorGris,
              ),
              title: Container()),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.supervised_user_circle,
                size: 30.0,
                color: colorBlanco,
              ),
              title: Container()),
          BottomNavigationBarItem(
              icon:
                  Icon(Icons.assignment_rounded, size: 30.0, color: colorGris),
              title: Container()),
          BottomNavigationBarItem(
              icon: Icon(Icons.watch_later_sharp, size: 30.0, color: colorGris),
              title: Container()),
        ],
        onTap: (int index) {
          // Datos personales
          if (index == 0) {
            Navigator.pushNamed(context, 'datospersonales');
          }

          // Datos laborales
          if (index == 1) {
            Navigator.pushNamed(context, 'datoslaborales');
          }

          // Solicitudes
          if (index == 2) {
            Navigator.pushNamed(context, 'solicitudes');
          }

          // Permisos
          if (index == 3) {
            Navigator.pushNamed(context, 'permisos');
          }
        },
        currentIndex: _selectedIndex,
        selectedItemColor: colorBlanco,
      ),
    );
  }
}
